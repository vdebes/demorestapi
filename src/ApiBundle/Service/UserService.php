<?php

namespace ApiBundle\Service;

use ApiBundle\Entity\User;
use ApiBundle\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService extends AbstractService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * UserService constructor.
     * @param ValidatorInterface $validator
     * @param UserRepository $userRepository
     */
    public function __construct(
        ValidatorInterface $validator,
        UserRepository $userRepository) {
        $this->validator = $validator;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return User
     * @throws \Exception
     */
    public function createResource(User $user)
    {
        try {
            $this->validateRessource($user);
            return $this->userRepository->save($user);
        } catch (\Exception $exception) {
            if ($exception instanceof UniqueConstraintViolationException) {
                throw new ConflictHttpException('User already exists');
            }

            throw $exception;
        }
    }

    /**
     * @param User $user
     * @throws BadRequestHttpException
     */
    private function validateRessource(User $user)
    {
        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($user);

        if ($errors->count() > 0) {
            $errorMessage = $this->getMessagesFromIterator($errors);
            throw new BadRequestHttpException($errorMessage);
        }
    }
}
