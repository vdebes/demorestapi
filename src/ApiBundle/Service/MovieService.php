<?php

namespace ApiBundle\Service;

use ApiBundle\Entity\Movie;
use ApiBundle\Repository\MovieRepository;
use ApiBundle\Repository\UserRepository;
use ApiBundle\Service\Helper\QueryHelper;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MovieService extends AbstractService
{
    const EXPECTED_QUERY_PARAMETERS = ['order', 'limit'];

    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var MovieRepository
     */
    private $movieRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * UserService constructor.
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param MovieRepository $movieRepository
     */
    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        MovieRepository $movieRepository) {
        $this->validator = $validator;
        $this->movieRepository = $movieRepository;
        $this->serializer = $serializer;
    }

    /**
     * @param array $queryParams
     * @return array
     */
    public function getAllResources(array $queryParams = [])
    {
        if (empty($queryParams)) {
            return $this->movieRepository->findAll();
        }

        QueryHelper::validateQueryParameters($queryParams, self::EXPECTED_QUERY_PARAMETERS);
        try {
            return $this->movieRepository->list($queryParams);
        } catch (OptimisticLockException $exception) {
            throw new ConflictHttpException();
        }
    }

    /**
     * @param Movie $movie
     * @return Movie
     * @throws \Exception
     */
    public function createResource(Movie $movie)
    {
        try {
            $this->validateRessource($movie);
            return $this->movieRepository->save($movie);
        } catch (\Exception $exception) {
            if ($exception instanceof UniqueConstraintViolationException) {
                throw new ConflictHttpException('Movie already exists');
            }

            throw $exception;
        }
    }

    /**
     * @param array $searchParameters
     */
    public function getSearchResults(array $searchParameters)
    {
        // @TODO implement method
    }

    /**
     * @param Movie $movie
     * @throws BadRequestHttpException
     */
    private function validateRessource(Movie $movie)
    {
        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($movie);

        if ($errors->count() > 0) {
            $errorMessage = $this->getMessagesFromIterator($errors);
            throw new BadRequestHttpException($errorMessage);
        }
    }
}
