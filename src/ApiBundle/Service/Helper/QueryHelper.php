<?php

namespace ApiBundle\Service\Helper;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class QueryHelper
{
    /**
     * Throw exception if unknown query parameters are used
     * @param array $actualQueryParameters
     * @param array $expectedQueryParameters
     */
    public static function validateQueryParameters(array $actualQueryParameters, array $expectedQueryParameters)
    {
        $diff = array_diff(array_keys($actualQueryParameters), $expectedQueryParameters);

        if ($diff) {
            throw new BadRequestHttpException('Invalid search parameter(s) : ' . implode(',', $diff));
        }
    }
}