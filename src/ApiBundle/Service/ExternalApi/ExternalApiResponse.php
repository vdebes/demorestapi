<?php


namespace ApiBundle\Service\ExternalApi;


class ExternalApiResponse
{
    /**
     * @var array
     */
    private $searchResults;
    /**
     * @var int
     */
    private $totalResults;

    /**
     * ExternalApiResponse constructor.
     */
    public function __construct(array $searchResults, int $totalResults)
    {
        $this->searchResults = $searchResults;
        $this->totalResults = $totalResults;
    }

    /**
     * @return array
     */
    public function getSearchResults(): array
    {
        return $this->searchResults;
    }

    /**
     * @return int
     */
    public function getTotalResults(): int
    {
        return $this->totalResults;
    }
}
