<?php


namespace ApiBundle\Service\ExternalApi;

use ApiBundle\Entity\Movie;
use ApiBundle\Service\Helper\QueryHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ExternalApiService
 * @package ApiBundle\Service
 */
class ExternalApiService
{
    const SEARCH_PARAMETERS_MAPPING = [
        'search' => 's',
        'year' => 'y'
    ];

    const API_BASE_URI = 'http://www.omdbapi.com/?apikey=16f220dc';
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ExternalApiService constructor.
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     */
    public function __construct(ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->validator = $validator;
        $this->logger = $logger;
    }


    /**
     * @param array $searchParameters
     * @return array
     */
    public function getSearchResults(array $searchParameters)
    {
        QueryHelper::validateQueryParameters($searchParameters, array_keys(self::SEARCH_PARAMETERS_MAPPING));
        $mappedSearchParameters = self::mapSearchParameters($searchParameters);
        $queryString = self::getQueryString($mappedSearchParameters);

        /** @var ExternalApiResponse $output */
        $output = (self::doRequest($queryString))->getSearchResults();

        $moviesFound = [];
        foreach ($output as $index => $movieArray) {
            try {
                $movie = new Movie();
                $movie->setTitle($movieArray['Title']);
                $movie->setImdbID($movieArray['imdbID']);
                $movie->setPoster($movieArray['Poster']);
                $movie->setYear($movieArray['Year']);

                if ($this->validator->validate($movie)) {
                    $moviesFound[] = $movie;
                }
            } catch (\Exception $exception) {
                $this->logger->log('error', $exception->getMessage());
            }
        }

        return $moviesFound;
    }

    /**
     * Sends the actual request and return the result
     * @param string $queryString
     * @return ExternalApiResponse
     * @throws ServiceUnavailableHttpException
     */
    private static function doRequest(string $queryString)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_BASE_URI . $queryString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
        $output = json_decode($output, true);

        if (!$output || !is_array($output)
            || !array_key_exists('Search', $output)
            || !array_key_exists('totalResults', $output)
        ) {
            throw new ServiceUnavailableHttpException(5, 'External data provider is down');
        }

        curl_close($ch);

        return new ExternalApiResponse($output['Search'], $output['totalResults']);
    }

    /**
     * Converts API search parameters names to external API search parameters names and keeps values
     * @param array $searchParameters
     * @return array
     */
    private static function mapSearchParameters(array $searchParameters)
    {
        $mappedParameters = [];
        $mapping = self::SEARCH_PARAMETERS_MAPPING;
        foreach ($searchParameters as $name => $value) {
            $mappedParameters[$mapping[$name]] = $value;
        }

        return $mappedParameters;
    }

    /**
     * Converts array of search parameters to a query string for GET request
     * @param array $searchParameters
     * @return string
     */
    private static function getQueryString(array $searchParameters)
    {
        $queryString = '';
        foreach ($searchParameters as $name => $value) {
            $queryString .= "&$name=$value";
        }

        return $queryString;
    }
}
