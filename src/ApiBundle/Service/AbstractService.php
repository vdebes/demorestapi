<?php


namespace ApiBundle\Service;

use LogicException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

abstract class AbstractService
{
    const RESPONSE_FORMAT = 'json';

    /**
     * @param ConstraintViolationList $violationList
     * @return string
     */
    protected static function getMessagesFromIterator(ConstraintViolationList $violationList) {
        $errorMessages = '';
        foreach ($violationList as $violation) {
            if (false === $violation instanceof ConstraintViolation) {
                throw new LogicException('Iterator should contain only elements of class '
                    . ConstraintViolation::class);
            }
            $errorMessages .= $violation->getMessage() . PHP_EOL;
        }

        return $errorMessages;
    }
}