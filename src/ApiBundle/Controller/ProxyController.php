<?php

namespace ApiBundle\Controller;

use ApiBundle\Service\ExternalApi\ExternalApiService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class ProxyController
 * @package ApiBundle\Controller
 * @Route(path="proxy/movies", name="_proxy_movie")
 */
class ProxyController extends Controller
{
    /**
     * @var ExternalApiService
     */
    private $externalApiService;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    /**
     * MovieRepository constructor.
     * @param ExternalApiService $externalApiService
     * @param NormalizerInterface $normalizer
     */
    public function __construct(
        ExternalApiService $externalApiService,
        NormalizerInterface $normalizer) {
        $this->externalApiService = $externalApiService;
        $this->normalizer = $normalizer;
    }

    /**
     * List all movies in local store or search if you add query parameters
     * @Route(path="/", name="_index", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        return $this->searchAction($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    private function searchAction(Request $request)
    {
        $queryParams = $request->query->getIterator()->getArrayCopy();
        $searchResults = $this->externalApiService->getSearchResults($queryParams);

        $response = new JsonResponse(['data' => $this->normalizer->normalize($searchResults)], Response::HTTP_OK);
        $response->headers->set('Location', 'movies');

        return $response;
    }
}
