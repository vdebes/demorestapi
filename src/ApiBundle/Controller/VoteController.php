<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VoteController
 * @package ApiBundle\Controller
 */
class VoteController extends Controller
{
    /**
     * @Route(name="user_movie_create", path="/vote")
     * @param Request $request
     */
    public function createAction(Request $request)
    {
        // validate post parameters
        // get movie from imdb id
        // persist movie
        // persist vote as assoc user-movie
    }
}