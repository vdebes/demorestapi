<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Movie;
use ApiBundle\Entity\User;
use ApiBundle\Form\UserType;
use ApiBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class UserController
 * @package ApiBundle\Controller
 * @Route(path="users", name="_user")
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    /**
     * UserController constructor.
     * @param UserService $userService
     * @param NormalizerInterface $normalizer
     */
    public function __construct(UserService $userService, NormalizerInterface $normalizer)
    {
        $this->userService = $userService;
        $this->normalizer = $normalizer;
    }

    /**
     * @Route(path="/", name="_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);

        $newUser = new User();
        $form = $this->createForm(UserType::class, $newUser);
        $form->submit($requestData);

        $newUser = $this->userService->createResource($newUser);

        $response = new JsonResponse(['data' => $this->normalizer->normalize($newUser)], Response::HTTP_OK);
        $response->headers->set('Location', 'users/' . $newUser->getLogin());

        return $response;
    }

    /**
     * @Route(path="/{login}", name="_show", methods={"GET"})
     *
     * @param User $user
     * @ParamConverter("user", class="ApiBundle\Entity\User")
     * @return JsonResponse
     */
    public function showAction(User $user)
    {
        $response = new JsonResponse(['data' => $this->normalizer->normalize($user)], Response::HTTP_OK);
        $response->headers->set('Location', 'users/' . $user->getLogin());

        return $response;
    }

    /**
     * @Route(path="/{login}/movies/", name="_vote", methods={"GET"})
     *
     * @param User $user
     */
    public function voteAction(User $user)
    {

    }
}
