<?php

namespace ApiBundle\Controller;

use ApiBundle\Service\ExternalApi\ExternalApiService;
use ApiBundle\Service\MovieService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class MovieController
 * @package ApiBundle\Controller
 * @Route(path="/movies", name="_movie")
 */
class MovieController extends Controller
{
    /**
     * @var MovieService
     */
    private $movieService;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    /**
     * MovieRepository constructor.
     * @param MovieService $movieService
     * @param NormalizerInterface $normalizer
     */
    public function __construct(
        MovieService $movieService,
        NormalizerInterface $normalizer) {
        $this->movieService = $movieService;
        $this->normalizer = $normalizer;
    }

    /**
     * List all movies in local store or search if you add query parameters
     * @Route(path="/", name="_index", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        $queryHasNoParameters = $request->query->count() === 0;
        if ($queryHasNoParameters) {
            return $this->listAction($request);
        }

        return $this->searchAction($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    private function listAction(Request $request)
    {
        $queryParams = $request->query->getIterator()->getArrayCopy();
        $allMovies = $this->movieService->getAllResources($queryParams);

        $response = new JsonResponse(['data' => $this->normalizer->normalize($allMovies)], Response::HTTP_OK);
        $response->headers->set('Location', 'movies');

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    private function searchAction(Request $request)
    {
        $queryParams = $request->query->getIterator()->getArrayCopy();
        $searchResults = $this->movieService->getSearchResults($queryParams);

        $response = new JsonResponse(['data' => $this->normalizer->normalize($searchResults)], Response::HTTP_OK);
        $response->headers->set('Location', 'movies');

        return $response;
    }
}
