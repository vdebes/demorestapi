<?php

namespace ApiBundle\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionSubscriber
 * Listens to thrown exceptions and converts the response into a JSON exception
 * @package ApiBundle\Subscriber
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [KernelEvents::EXCEPTION => 'onKernelException'];
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $customResponseData = [
            'success' => boolval($exception->getCode()),
            'message' => $exception->getMessage(),
            'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR
        ];

        if ($exception instanceof HttpException) {
            $customResponseData['statusCode'] = $exception->getStatusCode();
        }

        $customResponse = new JsonResponse(
            $customResponseData,
            $customResponseData['statusCode']
        );

        $event->setResponse($customResponse);
    }
}
