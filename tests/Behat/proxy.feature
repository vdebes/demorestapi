Feature: Querying external API through our proxy feature

  Scenario Outline: I search for a movie
    Given I search for a movie which title contains a <titlePart>
    Then If there are results their title contains <titlePart>
    Examples:
    | titlePart |
    | conan     |
